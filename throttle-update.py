#!/usr/bin/env python
__author__ = "Malcolm Gillies <gitorious@ouabain.org>"
__license__= "GPL v3"
__program__ = "rtorrent-modesty dynamic data volume management for rtorrent"

#    Copyright (C) 2011 Malcolm Gillies
#    Copyright (C) 2009-2011 dave b @ d1b.org

#    This file is part of rtorrent-modesty.
#
#    rtorrent-modesty is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    rtorrent-modesty is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with rtorrent-modesty.  If not, see <http://www.gnu.org/licenses/>.

from xml.dom import minidom
import urllib
import sys
import pycurl
import StringIO
import time, traceback
import getpass
import os
import os.path
import datetime

from volusage import VolUsage, VolUsageEstimate

from pyrocore import config
from pyrocore import error
from pyrocore.util import load_config

import logging
import logging.handlers
import ConfigParser

from flufl.lock import Lock
from datetime import timedelta

my_logger = None

def write_to_a_file(data,full_file_loc):
	the_file = open(full_file_loc, 'w')
	the_file.write(data)
	the_file.close()

def read_from_a_file(full_file_loc,return_type ):
	the_file = open(full_file_loc, 'r')
	if return_type == "read":
		return_type = the_file.read()
	elif return_type == "readlines":
		return_type =the_file.readlines()
	the_file.close()
	return return_type

def get_credentials_from_accounts_file(account_file_loc = os.path.expanduser("~/.iinet/settings/account")):
	credentials =[]
	username = ""
	password = ""
	the_file = read_from_a_file(account_file_loc, "readlines")
	for line in the_file:
		if "username=" in line:
			index = line.find("=")
			assert index +1 < len(line)
			username = line[index+1:-1]
		if "password=" in line:
			index = line.find("=")
			assert index +1 < len(line)
			password = line[index+1:-1]

	credentials.append(urllib.quote(username) )
	credentials.append(urllib.quote(password) )
	return credentials

def store_xml_cache(data,file_loc):
        write_to_a_file(data,file_loc)

def return_xml_cache(file_loc = os.path.expanduser("~/.iinet/data/data.xml")):
        return read_from_a_file(os.path.expanduser(file_loc), "read")

def return_diff_mod_time_current_time(file_loc):
        modification_file_time = (os.path.getmtime(file_loc))
        current_time = time.time()
        return (current_time - modification_file_time)

def check_if_local_cache_is_recent(file_loc):
        mod_diff =  return_diff_mod_time_current_time(file_loc)
        if mod_diff >= 1200: #20 minutes
                return False
        return True

def establish_ssl_connection(url):
	"""returns the contents of the request as as a string, if no error is thrown.
	the request can also be for an HTTP url.
	However the indended purpose is to retrieve data from a HTTPS url."""
	data = StringIO.StringIO()
	connection = pycurl.Curl()
	#*DO* *NOT* follow redirects.
	connection.setopt(pycurl.FOLLOWLOCATION, False)
	#verify ssl
	connection.setopt(pycurl.SSL_VERIFYPEER, 1)
	connection.setopt(pycurl.SSL_VERIFYHOST, 2)
	connection.setopt(pycurl.SSLVERSION, 3)

	connection.setopt(pycurl.WRITEFUNCTION, data.write)
	connection.setopt(pycurl.USERAGENT, "poc.py iinet client version 0.09")

	#set the url
	connection.setopt(pycurl.URL, url)
	connection.perform()

	assert connection.getinfo(pycurl.HTTP_CODE) == 200, "HTTP response was not 200 OK"
	#close the connection
	connection.close()
	return data.getvalue()

def set_from_url_XML_Feed(url):
	credentials = get_credentials_from_accounts_file()
	url = url + credentials[0] +'&action=login&password=' +  credentials[1]
	try:
		data_Container = establish_ssl_connection(url)
		return data_Container
	except AssertionError as e:
                my_logger.info(str(e))
	except pycurl.error as (n, e):
                my_logger.info(e)
	except Exception as e:
                my_logger.info(str(e))
	return "error"

def open_XML_Feed(data_Container):
	try:
		xml_tree = minidom.parseString(data_Container)
	except:
                my_logger.info("XML parse error")
		#http://code.google.com/p/iinet-api/issues/detail?id=12
		xml_tree = None
	return xml_tree

def anniversary(xml_tree):
	nodelist  = xml_tree.getElementsByTagName("anniversary")
	days = nodelist[0].firstChild.data
	return int(days)

def my_time(str):
	l = str.split(":")
	return((int(l[0])+int(l[1])/60)/24.0)

def offpeak(xml_tree):
	nodelist = xml_tree.getElementsByTagName("offpeak_start")
	start = my_time(nodelist[0].firstChild.data)
	nodelist = xml_tree.getElementsByTagName("offpeak_end")
	end = my_time(nodelist[0].firstChild.data)
	return((start,end))

def usage(xml_tree):
	nodelist  = xml_tree.getElementsByTagName("type")
        q = {}; u = {}
	for item in nodelist:
		c=item.getAttribute("classification")
		u[c] = int(item.getAttribute("used"))
		for items in item.childNodes:
			if items.localName == "quota_allocation":
				q[c] = int(items.firstChild.data)
        return (q,u)


def error_check(xml_tree):
	"""if the xml file is valid (does not contain an error tag and
	has a iinet xml tag (ii_feed) )) then None will be returned (*no* *error*).
	otherwise the function will return an error string."""
	if (xml_tree == None):
		return "No data returned by provider"
	try:
		nodelist  = xml_tree.getElementsByTagName("error")
	except: #no error tag is found in the xml.
		pass
	else:
		for item in nodelist:
			if item.hasChildNodes:
				try:
					error_to_record = str (item.firstChild.data)
				except:
					error_to_record = "Empty error message from provider"

				return(error_to_record)
	try:
		nodelist  = xml_tree.getElementsByTagName("ii_feed")
		for item in nodelist:
			if item.hasChildNodes:
				return None #no error.
	except: #no ii_feed tag is found in the xml.
		pass

	return "No ii_feed element found in data"

up_max = 412500 # theoretical max for ADSL2+ Annex M

def main():
	global my_logger
	logging.raiseExceptions = False

	my_logger = logging.getLogger(os.path.basename(sys.argv[0]))
	my_logger.setLevel(logging.INFO)

	handler = logging.handlers.SysLogHandler("/dev/log")

	formatter = logging.Formatter("%(name)s[%(process)d]: %(message)s")
	handler.setFormatter(formatter)

	my_logger.addHandler(handler)

	c = ConfigParser.RawConfigParser()
	c.read(os.path.expanduser("~/.rtorrent-modesty"))
	mulog = c.getfloat('Estimator', 'mulog')
	siglog = c.getfloat('Estimator', 'siglog')
	quantile = c.getfloat('Estimator', 'quantile')
	interval = c.getfloat('Estimator', 'interval')

        lock = Lock(os.path.expanduser("~/.rtorrent-modesty.lock"))
        lock.lifetime = timedelta(seconds=60)

        with lock:
		recent_local_copy = check_if_local_cache_is_recent(os.path.expanduser("~/.iinet/data/data.xml"))
		if recent_local_copy:
			data_Container = return_xml_cache()
			if data_Container == "":
				data_Container= "error"
		else:
			data_Container = set_from_url_XML_Feed('https://toolbox.iinet.net.au/cgi-bin/new/volume_usage_xml.cgi?username=')

		if data_Container == "error":
			sys.exit()

		if not recent_local_copy:
			store_xml_cache(data_Container,os.path.expanduser("~/.iinet/data/data.xml"))

	del lock

	xml_tree = open_XML_Feed(data_Container)
	err = error_check(xml_tree)
	if err:
		my_logger.info(err)
		sys.exit()

	(q,u) = usage(xml_tree)
	a = anniversary(xml_tree)
	o = offpeak(xml_tree)

	x = VolUsage(Qpeak=q["peak"]*1000*1000, \
	    Qoff=q["offpeak"]*1000*1000, s = o[0], f = o[1], D = a)

	if(x.o):
	    t = "offpeak"
	    z = VolUsageEstimate(mulog=None, interval=interval, weight=0.1)
	else:
	    t = "peak"
	    z = VolUsageEstimate(mulog=mulog, siglog=siglog, quantile=quantile, interval=interval, weight=0.25)

	s = z.estimate(x, u[t])
	# n.b. rtorrent uses conventional Kibibytes (1024 bytes) for throttle
	# iiNet uses decimal KB/MB/GB  (i.e. 1000 bytes etc)

	# theoretical max upload speed on Annex M is 403 KiB

	up = min(s, up_max)
	up = max(up, 1024)

	my_logger.info("%s, days left = %.2f, use = %d, est = %d, s = %d (%d)" % \
	    (t, x.rem, u[t]/(1000*1000), z.w/(1000*1000), up/1024, s/1024))

	try:
	    load_config.ConfigLoader().load()
	except error.LoggableError as exc:
	    my_logger.info("pyroscope setup failed: " + str(exc))
	    sys.exit()

	try:
	    proxy = config.engine.open()
	except error.LoggableError as exc:
	    my_logger.info(str(exc))
	    sys.exit()

	proxy.set_upload_rate(up)
	
if __name__=='__main__':
	main()
