#!/usr/bin/env python
__author__ = "Malcolm Gillies <gitorious@ouabain.org>"
__license__= "GPL v3"
__program__ = "cap.py -- rtorrent-modesty dynamic data volume management for rtorrent"

#    Copyright 2011 Malcolm Gillies

#    This file is part of rtorrent-modesty.
#
#    rtorrent-modesty is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    rtorrent-modesty is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with rtorrent-modesty.  If not, see <http://www.gnu.org/licenses/>.

import time

class VolUsage:
    """Volume usage calculator class"""
    daystosecs = 24*60*60
    secstodays = 1.0/daystosecs
    def __init__(self, Qpeak = 50000000000, Qoff = 50000000000,
        s = 2/24.0, f = 8/24.0, t = None, D = 16):
        self.Qpeak = Qpeak	# monthly quota in MB (peak)
        self.Qoff = Qoff	# monthly quota in MB (offpeak)
        self.s = s		# offpeak start (days since midnight localtime)
        self.f = f		# offpeak finish (days since midnight localtime)
        self.fr = ((s-f)%1,(f-s)%1) # fraction of day that is (peak, offpeak)

        # t now (days since epoch)
        if(t == None):
            self.t = time.time() * self.secstodays
        else:
            self.t = t

	# Check this is correct during DST crossover!
	ts = time.localtime(self.t / self.secstodays)
	# t24 now (days since midnight localtime)
	t24 = self.t - time.mktime((ts.tm_year, ts.tm_mon, ts.tm_mday, \
            0, 0, 0, 0, 0, -1)) * self.secstodays

        # D quota reset (in decimal days since epoch) -- midnight localtime
        if(ts.tm_mday < D):
            self.D = time.mktime((ts.tm_year, ts.tm_mon, D, \
                0, 0, 0, 0, 0, -1)) * self.secstodays
        elif(ts.tm_mon < 12):
            self.D = time.mktime((ts.tm_year, ts.tm_mon + 1, D, \
                0, 0, 0, 0, 0, -1)) * self.secstodays
        else:
            self.D = time.mktime((ts.tm_year + 1, 1, D, \
                0, 0, 0, 0, 0, -1)) * self.secstodays

	if self.s < t24 < self.f:
	    self.o = 1; l = self.f - t24
	elif t24 > f:
	    self.o = 0; l = 1 - t24
	else:
	    self.o = 0; l = 1 - t24 - (self.f - self.s)

	# days remaining

	self.rem = l + self.fr[self.o] * int(self.D - self.t)

class VolUsageEstimate:
    """Volume usage estimator class"""
    def __init__(self, mulog=18.26084245, siglog=1.54143495, quantile=0.96, \
        interval=20.0/(24*60), weight=0):
        self.mulog=mulog
        self.siglog=siglog
        self.quantile=quantile
        self.interval=interval
        self.weight=weight

    def ppfn(self, n):
        from math import log, exp, sqrt
        from scipy.stats import lognorm
        if self.mulog is not None:
            siglogp = sqrt(log((exp(self.siglog*self.siglog)-1)/n+1))
            mulogp = self.mulog + log(n) + self.siglog*self.siglog/2 - siglogp**2/2
	    rv = lognorm(siglogp,scale=exp(mulogp))
            return(rv.ppf(self.quantile))
        else:
            return(0)

    def estimate(self, q, u):
        d = q.rem
        # print "d", d
        dn = d / q.fr[q.o]
        # print "dn", dn
        if q.o:
            r = q.Qoff
        else:
            r = q.Qpeak
        r = r - u
        # print "r - u", r
        a = self.ppfn(int(dn)+1)
        if int(dn):
            b = self.ppfn(int(dn))
        else:
            b = 0
        # print "a", a, "b", b
        f = dn - int(dn)
        w = f * a + (1 - f) * b
        self.w = w
        # print "w", w
        r = r - w
        s = max(d * q.daystosecs, 1) # avoid divide by zero
        ss = self.interval * q.daystosecs
        s = min(s, ss)	# unnecessary, but why not?
        if r < 0:
            return(0)
        else:
            return(int(self.weight * (r/s) + (1-self.weight) * (r/ss)))

def main():
    import random

    x = VolUsage()
    sum = [0, 0]
    n = [0, 0]
    tt = 0

    for i in range(1, 24):
	D = time.mktime((2011, 9, 15, 0, 0, 0, 0, 0, -1)) * x.secstodays
	r = 30 * random.random()
	t = D - r
	y = VolUsage(D = D, t = t)
	z = VolUsageEstimate(weight=1.0)
	u = 50000000000 * random.random()
	s = z.estimate(y,u)
	print "%.2f, %d, %.2f, %d, %d, %d" % (r, y.o, y.rem, u/1000000, \
	    z.w/1000000, s/1000)

if __name__ == '__main__':
    main()
