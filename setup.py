#!/usr/bin/env python

from distutils.core import setup

setup(name='rtorrent-modesty',
      version='0.1',
      description='Dynamic bandwidth management for rtorrent bittorrent uploads',
      author='Malcolm Gillies',
      author_email='gitorious@ouabain.org',
      url='http://gitorious.org/rtorrent-modesty',
      py_modules=['volusage'],
      scripts=['throttle-update.py'],
     )
